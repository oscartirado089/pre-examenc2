import express from "express";

export const router = express.Router();

export default { router };

// Configurar primer ruta
router.get("/", (req, res) => {
  res.render("index", { titulo: "Pre-examen Corte 2" });
});

router.get("/pago", (req, res) => {
  const params = {
    titulo: "Pre-examen Corte 2",
    numero: req.query.numero,
    nombre: req.query.nombre,
    domicilio: req.query.domicilio,
    servicio: req.query.servicio,
    kwConsumidos: req.query.kwConsumidos,
    isPost: false,
  };
  res.render("pago", params);
});

router.post("/pago", (req, res) => {
  const precios = [1.08, 2.5, 3.0];

  const { numero, nombre, domicilio, servicio, kwConsumidos } = req.body;
  const precioKw = precios[servicio * 1];
  const tipoDeServicio = servicio == 0 ? 'Domestico' :  servicio == 1 ? 'Comercial' : 'Industrial'
  const subtotal = (parseFloat(precioKw) * parseFloat(kwConsumidos)).toFixed(2);

  // Calcular el descuento
  let descuento = 0;
  if (kwConsumidos <= 1000) {
    descuento = 0.1;
  } else if (kwConsumidos > 1000 && kwConsumidos <= 10000) {
    descuento = 0.2;
  } else {
    descuento = 0.5;
  }

  // Aplicar el descuento al subtotal
  const descuentoAplicado = (parseFloat(subtotal) * parseFloat(descuento)).toFixed(2);
  const subtotalConDescuento = subtotal - descuentoAplicado;

  // Calcular el impuesto
  const impuesto = (subtotal * 0.16).toFixed(2);

  // Calcular el total a pagar
  const total = (parseFloat(subtotalConDescuento) + parseFloat(impuesto)).toFixed(2);

  // Actualizar el objeto 'resultado'
  const params = {
    titulo: "Pre-examen Corte 2",
    numero,
    nombre,
    domicilio,
    servicio: tipoDeServicio,
    kwConsumidos,
    precioKw,
    subtotal,
    descuento: descuentoAplicado, // Actualizado para reflejar el descuento aplicado
    subtotalConDescuento,
    impuesto,
    total,
    isPost: true,
  };
  res.render("pago", params);
});

router.get("/contacto", (req, res) => {
    res.render("contacto", { titulo: "Pre-examen Corte 2" });
});